package team.team428.notepad28;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;
import team.team428.notepad28.Service.ServiceInterface;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DetailScheduleActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    protected int scheduleId;
    protected Schedule schedule;
    protected ServiceInterface serviceInterface = ServiceInterface.getServiceInstance(this);

    private EditText etScheduleName;
    private EditText etScheduleContent;
    private TextView tvStartTime;
    private TextView tvEndTime;
    private ListView listView;
    private ImageButton imageButton;
    private List<Reminder> reminders;
    private int fresh = -1;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_layout);

        etScheduleName = findViewById(R.id.et_schedule_name_picker);
        etScheduleContent = findViewById(R.id.et_schedule_content_picker);
        tvStartTime = findViewById(R.id.et_schedule_start_time_picker);
        tvEndTime = findViewById(R.id.tv_schedule_end_time_picker);
        listView = findViewById(R.id.lv_reminders);
        imageButton = findViewById(R.id.ib_add_reminder);
        schedule = new Schedule(null, LocalDateTime.now(), LocalDateTime.now(), null);
        reminders = new ArrayList<>();

        scheduleId = getIntent().getIntExtra("ScheduleID", -1);
        if (scheduleId != -1) {
            schedule = serviceInterface.findScheduleById(scheduleId);
            etScheduleName.setText(schedule.getName());
            etScheduleContent.setText(schedule.getContent());
            tvStartTime.setText(schedule.getBeginTime().toString());
            tvEndTime.setText(schedule.getEndTime().toString());
            reminders = serviceInterface.findReminderBySchedule(schedule);
            refreshListView();
        }

        //startTime的新数据获取
        tvStartTime.setOnTouchListener((view, motionEvent) -> {
            //依次启动日期选择和事件选择，将选择后的数据显示在相应的textView中
            new DatePickerDialog(DetailScheduleActivity.this, (datePicker, year, mouth, day) ->
                    new TimePickerDialog(DetailScheduleActivity.this, (timePicker, hour, minute) -> {
                        schedule.setBeginTime(LocalDateTime.of(year, mouth + 1, day, hour, minute));
                        tvStartTime.setText(schedule.getBeginTime().toString());
                    }
                            , schedule.getBeginTime().getHour()
                            , schedule.getBeginTime().getMinute()
                            , true).show()
                    , schedule.getBeginTime().getYear()
                    , schedule.getBeginTime().getMonthValue() - 1
                    , schedule.getBeginTime().getDayOfMonth()).show();
            return false;
        });

        imageButton.setOnClickListener(this);

        //endTime的新数据获取
        tvEndTime.setOnTouchListener((view, motionEvent) -> {
            new DatePickerDialog(DetailScheduleActivity.this, (datePicker, year, mouth, day) ->
                    new TimePickerDialog(DetailScheduleActivity.this, (timePicker, hour, minute) -> {
                        schedule.setEndTime(LocalDateTime.of(year, mouth + 1, day, hour, minute));
                        tvEndTime.setText(schedule.getEndTime().toString());
                    }
                            , schedule.getEndTime().getHour()
                            , schedule.getEndTime().getMinute()
                            , true).show()
                    , schedule.getEndTime().getYear()
                    , schedule.getEndTime().getMonthValue() - 1
                    , schedule.getEndTime().getDayOfMonth()).show();
            return false;
        });

        //长按后删除Reminder
        listView.setOnItemLongClickListener((adapterView, view, i, l) -> {
            new AlertDialog.Builder(this)
                    .setTitle("删除提醒")
                    .setMessage("是否确认删除该提醒")
                    .setPositiveButton("确认", (dialogInterface, i1) -> {
                        reminders.remove(i);
                        refreshListView();
                    })
                    .setNegativeButton("取消", null)
                    .create().show();
            return true;
        });

        //单击后修改reminder的内容
        listView.setOnItemClickListener(this);
    }

    //点击返回后询问是否保存
    @Override
    public void onBackPressed() {
        schedule.setName(String.valueOf(etScheduleName.getText()));
        schedule.setContent(String.valueOf(etScheduleContent.getText()));
        if (scheduleId == -1) {
            new AlertDialog.Builder(this)
                    .setTitle("是否要保存")
                    .setPositiveButton("保存", ((dialogInterface, i) -> {
                        if (!etScheduleName.getText().toString().equals("")) {
                            serviceInterface.insertSchedule(schedule);
                            serviceInterface.insertReminder(reminders, schedule);
                            super.onBackPressed();
                        } else {
                            new AlertDialog.Builder(this)
                                    .setTitle("日程名不能为空")
                                    .setPositiveButton("确认", null).create().show();
                        }
                    }))
                    .setNegativeButton("不保存", (dialogInterface, i) -> DetailScheduleActivity.super.onBackPressed())
                    .setNeutralButton("取消", null).create().show();
        } else if (!serviceInterface.findScheduleById(scheduleId).toString().equals(schedule.toString()) || fresh > 0) {
            new AlertDialog.Builder(this)
                    .setTitle("是否要保存")
                    .setPositiveButton("保存", ((dialogInterface, i) -> {
                        if (!etScheduleName.getText().toString().equals("")) {
                            serviceInterface.updateSchedule(serviceInterface.findScheduleById(scheduleId), schedule);
                            serviceInterface.insertReminder(reminders, schedule);
                            super.onBackPressed();
                        } else {
                            new AlertDialog.Builder(this)
                                    .setTitle("日程名不能为空")
                                    .setPositiveButton("确认", null).create().show();
                        }
                    }))
                    .setNegativeButton("不保存", (dialogInterface, i) -> DetailScheduleActivity.super.onBackPressed())
                    .setNeutralButton("取消", null).create().show();
        } else {
            super.onBackPressed();
        }
    }

    //刷新listView
    public void refreshListView() {
        List<String> data = new ArrayList<>();
        for (Reminder reminder : reminders) {
            data.add(reminder.getName() + ":" + reminder.getTime().toString());
        }
        ListAdapter adapter = new ArrayAdapter<>(DetailScheduleActivity.this, android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
        fresh += 1;
    }

    @Override
    public void onClick(View view) {

        EditText editText = new EditText(this);
        new AlertDialog.Builder(DetailScheduleActivity.this)
                .setTitle("请输入提醒名")
                .setView(editText)
                .setPositiveButton("确认", (dialogInterface, i) -> new DatePickerDialog(DetailScheduleActivity.this
                        , (datePicker, year, mouth, day) -> new TimePickerDialog(DetailScheduleActivity.this, (timePicker, hour, minute) -> {
                    reminders.add(new Reminder(editText.getText().toString()
                            , schedule.getId()
                            , LocalDateTime.of(year, mouth + 1, day, hour, minute),
                            null));
                    refreshListView();
                }
                        , LocalDateTime.now().getHour()
                        , LocalDateTime.now().getMinute()
                        , true).show()
                        , LocalDateTime.now().getYear()
                        , LocalDateTime.now().getMonthValue() - 1
                        , LocalDateTime.now().getDayOfMonth()).show())
                .setNegativeButton("取消", null).create().show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        EditText editText = new EditText(this);
        editText.setText(reminders.get(i).getName());
        Log.i("test", "onItemClick: ");
        new AlertDialog.Builder(DetailScheduleActivity.this)
                .setTitle("请输入提醒名")
                .setView(editText)
                .setPositiveButton("确认", (dialogInterface, i1) -> new DatePickerDialog(DetailScheduleActivity.this, (datePicker, year, mouth, day) -> new TimePickerDialog(DetailScheduleActivity.this, (timePicker, hour, minute) -> {
                    reminders.get(i).setTime(LocalDateTime.of(year, mouth + 1, day, hour, minute));
                    reminders.get(i).setName(editText.getText().toString());
                    refreshListView();
                }
                        , reminders.get(i).getTime().getHour()
                        , reminders.get(i).getTime().getMinute()
                        , true).show()
                        , reminders.get(i).getTime().getYear()
                        , reminders.get(i).getTime().getMonthValue() - 1
                        , reminders.get(i).getTime().getDayOfMonth()).show())
                .setNegativeButton("取消", null)
                .create().show();

    }
}
