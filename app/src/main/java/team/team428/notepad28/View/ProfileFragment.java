package team.team428.notepad28.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import team.team428.notepad28.R;

import java.util.Objects;

public class ProfileFragment extends Fragment {

    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity())
                .inflate(R.layout.profile_fragement_layout, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        button = Objects.requireNonNull(getActivity()).findViewById(R.id.button);
        button.setOnClickListener(view -> new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setTitle("关于")
                .setMessage("开发人员：徐奇，刘晨琪，徐济蒙，徐立华\n版本号：" + getActivity().getString(R.string.app_version))
                .setPositiveButton("确定", null)
                .create().show());
    }

    //重写setMenuVisibility()方法，否则会出现叠层的现象
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (this.getView() != null) {
            this.getView().setVisibility(menuVisible ? View.VISIBLE : View.GONE);
        }   //if
    }   //setMenuVisibility()
}   //ProfileFragment
