package team.team428.notepad28.View;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import team.team428.notepad28.Bean.Schedule;
import team.team428.notepad28.DetailScheduleActivity;
import team.team428.notepad28.R;
import team.team428.notepad28.Service.ServiceInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class HomeFragment extends Fragment {
    protected static final String TAG = "HomeFragment";

    private ListView listView;
    private Button button;
    private SearchView searchView;
    private ServiceInterface serviceInterface;
    private List<Schedule> scheduleAtList;
    List<String> data;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity())
                .inflate(R.layout.home_fragement_layout, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //该部分代码须在activity启动后运行，否则会因缺少相关activity，造成程序闪退
        listView = Objects.requireNonNull(getActivity()).findViewById(R.id.lv_view);
        button = Objects.requireNonNull(getActivity()).findViewById(R.id.schedule_add_button);
        searchView = getActivity().findViewById(R.id.search_view);
        serviceInterface = ServiceInterface.getServiceInstance(getActivity());
        serviceInterface.startAlarm();
        refreshListView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                refreshListView(serviceInterface.findScheduleByName(s));
                return false;
            }
        });

        listView.setOnItemLongClickListener((adapterView, view, i, l) -> {
            new AlertDialog.Builder(getActivity())
                    .setTitle("删除日程")
                    .setMessage("是否确认删除该日程")
                    .setPositiveButton("确认", (dialogInterface, i1) -> {
                        Log.i(TAG, "onItemClick: " +scheduleAtList.get(i).toString());
                        serviceInterface.deleteSchedule(scheduleAtList.get(i));
                        refreshListView();
                    })
                    .setNegativeButton("取消", null)
                    .create().show();
            return true;
        });

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            //易产生bug
            Intent intent = new Intent(getActivity(), DetailScheduleActivity.class);
            intent.putExtra("ScheduleID", scheduleAtList.get(i).getId());
            startActivity(intent);
        });

        button.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), DetailScheduleActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshListView();
    }

    //重写setMenuVisibility()方法，否则会出现叠层的现象
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (this.getView() != null) {
            this.getView().setVisibility(menuVisible ? View.VISIBLE : View.GONE);
        }   //if
    }   //setMenuVisibility()

    /**
     * 实时更新listView中的数据,采用变化即刷新的方式进行
     * 感觉不是正式的方法，后期尝试改为单一对应的方式，尝试调用listView自己的deleteItem和插入等相关函数
     */
    public void refreshListView() {
        scheduleAtList = serviceInterface.findAllSchedule();
        data = new ArrayList<>();
        for (Schedule schedule : scheduleAtList) {
            data.add(schedule.getName());
        }
        ListAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
    }

    public void refreshListView(List<Schedule> schedules) {
        scheduleAtList = schedules;
        data = new ArrayList<>();
        for (Schedule schedule : schedules) {
            data.add(schedule.getName());
        }
        ListAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
    }
}   //HomeFragment
