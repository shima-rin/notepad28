package team.team428.notepad28.Service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;
import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;
import team.team428.notepad28.DAO.DAOFactory;
import team.team428.notepad28.DAO.ReminderDAO;
import team.team428.notepad28.DAO.ScheduleDAO;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class AlarmService extends Service {

    protected Reminder currentReminder;     //当前待发出的提醒
    protected AlarmManager alarmManager;    //alarmManager的系统服务
    protected PendingIntent pendingIntent;  //AlarmManager待启动的intent
    protected Intent intentForNotification; //启动ReminderBroadcastReceiver的intent
    protected ReminderDAO reminderDAO;      //reminder接口
    protected ScheduleDAO scheduleDAO;      //schedule接口


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        intentForNotification = new Intent(this, RemindBroadcastReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, intentForNotification,
                PendingIntent.FLAG_UPDATE_CURRENT);
        reminderDAO = DAOFactory.getReminderDaoInstance();
        scheduleDAO = DAOFactory.getScheduleDaoInstance();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        checkCurrentRemind();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * 改变当前的reminder
     *
     * @param reminder 目标reminder
     */
    private void setCurrentReminder(Reminder reminder) {
        if (reminder == null) {
            stopSelf();
        } else {
            this.currentReminder = reminder;
            Schedule schedule = scheduleDAO.findByReminder(reminder);
            intentForNotification.putExtra("title", schedule.getName());
            intentForNotification.putExtra("text", schedule.getContent());
            intentForNotification.putExtra("reminderName", reminder.getName());
            pendingIntent = PendingIntent.getBroadcast(this, 0, intentForNotification,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, reminder.getTime().toInstant(ZoneOffset.of("+8")).toEpochMilli(),
                    pendingIntent);
        }

    }

    /**
     * 查看当前的提醒是否为正确的提醒
     */
    private void checkCurrentRemind() {
        //根据当前时间获取正确的reminder
        Reminder tempReminder = reminderDAO.findNext(LocalDateTime.now());
        //判断当前库中是否存在需要发出的提醒，若不存在则将当前的提醒置空,若存在则将其与当前的提醒进行比较
        if (tempReminder == null) {
            setCurrentReminder(null);
        } else if (currentReminder == null || !currentReminder.toString().equals(tempReminder.toString())) {
            setCurrentReminder(tempReminder);
        }

    }
}
