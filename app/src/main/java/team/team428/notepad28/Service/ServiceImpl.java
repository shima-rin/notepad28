package team.team428.notepad28.Service;

import android.content.Context;
import android.content.Intent;
import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;
import team.team428.notepad28.DAO.DAOFactory;
import team.team428.notepad28.DAO.ReminderDAO;
import team.team428.notepad28.DAO.ScheduleDAO;

import java.util.List;

public class ServiceImpl implements ServiceInterface {
    private final ReminderDAO reminderDAO = DAOFactory.getReminderDaoInstance();
    private final ScheduleDAO scheduleDAO = DAOFactory.getScheduleDaoInstance();
    private final Context context;

    public ServiceImpl(Context context) {
        this.context = context;
    }

    /**
     * 启动并保存context
     */
    @Override
    public void startAlarm() {
        Intent intent = new Intent(context, AlarmService.class);
        context.startService(intent);
    }

    @Override
    public void insertReminder(Reminder reminder, Schedule schedule) {
        reminderDAO.insert(reminder, schedule);
        startAlarm();
    }

    @Override
    public void insertReminder(List<Reminder> reminders, Schedule schedule) {
        reminderDAO.insert(reminders, schedule);
        startAlarm();
    }

    @Override
    public void insertSchedule(Schedule schedule) {
        scheduleDAO.insert(schedule);
    }

    @Override
    public void deleteReminder(Reminder reminder) {
        reminderDAO.delete(reminder);
        startAlarm();
    }

    @Override
    public void deleteReminder(List<Reminder> reminders) {
        reminderDAO.delete(reminders);
        startAlarm();
    }

    @Override
    public List<Reminder> findReminderBySchedule(Schedule schedule) {
        return reminderDAO.getRemindersBySchedule(schedule);
    }

    @Override
    public void deleteSchedule(Schedule schedule) {
        scheduleDAO.delete(schedule);
        startAlarm();
    }

    @Override
    public void deleteSchedule(List<Schedule> schedules) {
        scheduleDAO.delete(schedules);
        startAlarm();
    }

    @Override
    public void updateSchedule(Schedule oldOne, Schedule newOne) {
        scheduleDAO.update(oldOne, newOne);
        startAlarm();
    }

    @Override
    public Schedule findScheduleByReminder(Reminder reminder) {
        return scheduleDAO.findByReminder(reminder);
    }

    @Override
    public List<Schedule> findScheduleByName(String name) {
        return scheduleDAO.findByName(name);
    }

    @Override
    public Schedule findScheduleById(int ID) {
        return scheduleDAO.findById(ID);
    }

    @Override
    public List<Schedule> findAllSchedule() {
        return scheduleDAO.findAll();
    }

    @Override
    public List<Schedule> findAllScheduleOrderByStartTime() {
        return scheduleDAO.findAllOrderByStartTime();
    }
}
