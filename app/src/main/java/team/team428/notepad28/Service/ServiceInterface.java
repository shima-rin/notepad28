package team.team428.notepad28.Service;

import android.content.Context;
import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;

import java.util.List;

//当前类为接口类，用于添加提醒，添加提醒后要检查当前的提醒是否为最新的提醒
public interface ServiceInterface {
    /**
     * 获取接口实例
     *
     * @param context 使用接口的对象
     * @return 接口实例
     */
    static ServiceInterface getServiceInstance(Context context) {
        return new ServiceImpl(context);
    }

    /**
     * 该函数用于启动AlarmService
     */
    void startAlarm();

    /**
     * 插入一条Reminder
     *
     * @param reminder 目标reminder
     */
    void insertReminder(Reminder reminder,Schedule schedule);

    /**
     * 将schedule的所有reminder变为新的list
     * @param reminders 新的reminder
     * @param schedule  对应的schedule
     */
    void insertReminder(List<Reminder> reminders, Schedule schedule);

    /**
     * 插入一条Schedule
     *
     * @param schedule 目标schedule
     */
    void insertSchedule(Schedule schedule);

    /**
     * 删除一条Reminder
     *
     * @param reminder 目标reminder
     */
    void deleteReminder(Reminder reminder);

    /**
     * 批量删除reminder
     *
     * @param reminders 目标reminder列表
     */
    void deleteReminder(List<Reminder> reminders);

    /**
     * 查询一个日程的所有提醒
     * @param schedule 目标日程
     */
    List<Reminder> findReminderBySchedule(Schedule schedule);

    /**
     * 删除一条Schedule
     *
     * @param schedule 目标schedule
     */
    void deleteSchedule(Schedule schedule);

    /**
     * 批量删除schedule
     *
     * @param schedules 目标schedule列表
     */
    void deleteSchedule(List<Schedule> schedules);

    /**
     * 更新一条Schedule
     *
     * @param oldOne 源schedule
     * @param newOne 目标schedule
     */
    void updateSchedule(Schedule oldOne, Schedule newOne);

    Schedule findScheduleByReminder(Reminder reminder);

    /**
     * 根据关键词模糊查询
     *
     * @param name 关键字
     * @return 所有找到的Schedule
     */
    List<Schedule> findScheduleByName(String name);

    /**
     * 根据id查找schedule
     *
     * @param ID 目标id
     * @return 结果schedule
     */
    Schedule findScheduleById(int ID);

    /**
     * 列出所有日程，按照插入顺序
     *
     * @return 所有日程
     */
    List<Schedule> findAllSchedule();

    /**
     * 列出所有日程，按照起始时间排序
     *
     * @return 排序后的所有日程
     */
    List<Schedule> findAllScheduleOrderByStartTime();
}
