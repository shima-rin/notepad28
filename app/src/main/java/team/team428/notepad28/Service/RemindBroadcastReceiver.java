package team.team428.notepad28.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import team.team428.notepad28.R;

public class RemindBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String title = intent.getStringExtra("title");
        String text = intent.getStringExtra("text");
        String reminderName = intent.getStringExtra("reminderName");

        //发送通知
        String id = "ReminderNotification"; //通知id
        String description = "The only Notification for AlarmService!"; //通知内容描述
        int importance = NotificationManager.IMPORTANCE_DEFAULT;    //该重要度下开启通知，不会弹出，发出提示音，状态栏中显示
        NotificationChannel channel = new NotificationChannel(id, description, importance);
        manager.createNotificationChannel(channel);
        Notification notification = new Notification.Builder(context, id)
                .setCategory(Notification.CATEGORY_MESSAGE)         //通知类别
                .setSmallIcon(R.mipmap.ic_launcher)                 //通知小图标
                .setContentTitle("日程:" + title + ":" + text)      //通知标题
                .setContentText(reminderName)                       //通知内容
                .setAutoCancel(true)                                //通知点击后自动消失
                .build();
        manager.notify(1, notification);

        //更新AlarmService的信息
        Intent intentForService = new Intent(context, AlarmService.class);
        context.startService(intentForService);
    }
}
