package team.team428.notepad28.Bean;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

import java.time.LocalDateTime;

import team.team428.notepad28.DAO.Utils;

public class Reminder extends LitePalSupport{
    public Reminder(String name, int scheduleId, LocalDateTime time, String content) {
        this(name,scheduleId,Utils.LocalDateTimeToString(time),content);
    }

    @Deprecated
    private Reminder(String name, int scheduleId, String time, String content) {
        this.id = 0;
        this.name = name;
        this.scheduleId = scheduleId;
        this.time = time;
        this.content = content;
    }

    @Column(index = true)
    private int id;
    private String name;
    @Column(nullable = false)
    private int scheduleId;
    private String time;
    @Deprecated
    @Column(ignore = true)
    private String content;

    //删除了content
    @Override
    public String toString() {
        return "Reminder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", scheduleId=" + scheduleId +
                ", time='" + time + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public LocalDateTime getTime() {
        return Utils.StringToLocalDateTime(time);
    }

    public void setTime(LocalDateTime time) {
        this.time = Utils.LocalDateTimeToString(time);
    }

    @Deprecated
    public String getContent() {
        return content;
    }

    @Deprecated
    public void setContent(String content) {
        this.content = content;
    }
}
