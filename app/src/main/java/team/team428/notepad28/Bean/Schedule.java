package team.team428.notepad28.Bean;

import org.litepal.crud.LitePalSupport;

import java.time.LocalDateTime;

import team.team428.notepad28.DAO.Utils;

public class Schedule extends LitePalSupport{
    private int id;

    public Schedule(String name, LocalDateTime beginTime, LocalDateTime endTime, String content) {
        this(name,Utils.LocalDateTimeToString(beginTime),Utils.LocalDateTimeToString(endTime),content);
    }

    private Schedule(String name, String beginTime, String endTime, String content) {
        this.id = 0;
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.content = content;
    }

    private String name;
    private String beginTime;
    private String endTime;
    private String content;

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", beginTime='" + beginTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getBeginTime() {
       return Utils.StringToLocalDateTime(beginTime);
    }

    public void setBeginTime(LocalDateTime beginTime) {
        this.beginTime = Utils.LocalDateTimeToString(beginTime);
    }

    public LocalDateTime getEndTime() {
        return Utils.StringToLocalDateTime(endTime);
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = Utils.LocalDateTimeToString(endTime);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
