package team.team428.notepad28;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import team.team428.notepad28.View.HomeFragment;
import team.team428.notepad28.View.ProfileFragment;

public class MainActivity extends AppCompatActivity {
    private FrameLayout homeContent;    //导航栏上方的框架布局
    private RadioGroup radioGroup;      //导航栏放置2个按钮的容器

    //导航栏2个按钮
    private RadioButton rbHome, rbProfile;
    static final int NUM_ITEMS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }//初始化界面

    /**
     *  初始化界面函数
     */
    protected void initView() {
        //通过findViewById()方法对定义的4个控件进行初始化
        homeContent = findViewById(R.id.homeContent);
        radioGroup = findViewById(R.id.radioGroup);
        rbHome = findViewById(R.id.rbHome);
        rbProfile = findViewById(R.id.rbProfile);

        //为底部的RadioGroup绑定状态改变的监听事件
        //onCheckedChanged()
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            //为2个不同的选项设置不同的标量
            int index = 0;
            switch (checkedId) {
                case R.id.rbHome:
                    index = 0;
                    break;

                case R.id.rbProfile:
                    index = 1;
                    break;
            }
            //根据选中的Fragment更新homeContent
            updateHomeContent(index);
        });
    }   //initView()

    FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
        //根据传递过来的索引值对Fragment进行初始化
        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 1: //“我的”选项
                    fragment = new ProfileFragment();
                    break;
                case 0: //“首页”选项
                default:
                    fragment = new HomeFragment();
            }

            return fragment;    //返回初始化后的fragment
        }   //getItem()

        @Override
        public int getCount() {
            return NUM_ITEMS;   //一共有2个Fragment
        }
    };  //FragmentStatePagerAdapter

    void updateHomeContent(int i) {
        //通过fragment这个Adapter以及index来替换FragmentLayout中的内容
        Fragment fragment = (Fragment) adapter.instantiateItem(homeContent, i);

        //一开始将帧布局中的内容设置为第一个
        adapter.setPrimaryItem(homeContent, 0, fragment);

        //设置回调，完成更新
        adapter.finishUpdate(homeContent);
    }

    protected void onStart() {
        super.onStart();
        radioGroup.check(R.id.rbHome);

        //加载HomeFragment
        updateHomeContent(0);
    }
}   //MainActivity