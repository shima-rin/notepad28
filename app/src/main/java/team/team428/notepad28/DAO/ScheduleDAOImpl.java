package team.team428.notepad28.DAO;

import org.litepal.LitePal;

import java.util.List;

import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;

public class ScheduleDAOImpl implements ScheduleDAO {
    //    checked
    @Override
    public void insert(Schedule schedule) {
        schedule.save();
    }

    //    checked
    @Override
    public void delete(Schedule schedule) {
        schedule.delete();
    }

    // TODO: 2021/12/7 check 
    //sqlite不支持事务嵌套，子事务的begin end会被忽略
    @Override
    public void delete(List<Schedule> schedules) {
        try {
            LitePal.beginTransaction();
            for (Schedule schedule : schedules
            ) {
                schedule.delete();
            }
            LitePal.setTransactionSuccessful();
        } finally {
            LitePal.endTransaction();
        }
    }

    //    checked
    @Override
    public void update(Schedule oldOne, Schedule newOne) {
        oldOne.setName(newOne.getName());
        oldOne.setBeginTime(newOne.getBeginTime());
        oldOne.setEndTime(newOne.getEndTime());
        oldOne.setContent(newOne.getContent());
        oldOne.save();
    }

    @Override
    public Schedule findByReminder(Reminder reminder) {
        return LitePal.where("id = ?", Integer.toString(reminder.getScheduleId())).find(Schedule.class).get(0);
    }

    //    checked
    @Override
    public List<Schedule> findByName(String name) {
        return LitePal.where("name like ?", "%" + name + "%").find(Schedule.class);
    }

    //    checked
    @Override
    public List<Schedule> findAll() {
        return LitePal.findAll(Schedule.class);
    }

    // TODO: 2021/12/7 check
    @Override
    public List<Schedule> findAllOrderByStartTime() {
        return LitePal.order("begintime").find(Schedule.class);
    }

    // TODO: 2021/12/15 check 
    @Override
    public Schedule findById(int id) {
        return LitePal.where("id=?", String.valueOf(id)).find(Schedule.class).get(0);
    }
}
