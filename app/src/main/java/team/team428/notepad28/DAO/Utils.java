package team.team428.notepad28.DAO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Utils {
    public static String LocalDateTimeToString(LocalDateTime localDateTime){
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        return sdf.format(localDateTime.getTime());
        DateTimeFormatter dateTimeFormatter= DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return localDateTime.format(dateTimeFormatter);
    }

    public static LocalDateTime StringToLocalDateTime(String string) {
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        LocalDateTime localDateTime = LocalDateTime.getInstance();
//        try {
//            localDateTime.setTime(sdf.parse(string));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return localDateTime;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(string, dateTimeFormatter);
    }
}
