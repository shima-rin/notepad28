package team.team428.notepad28.DAO;

import java.time.LocalDateTime;
import java.util.List;

import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;

public interface ReminderDAO {
    /**
     * 插入一条提醒
     * @param reminder
     */
    void insert(Reminder reminder,Schedule schedule);

    void insert(List<Reminder> reminders,Schedule schedule);

    /**
     * 删除一条提醒
     * @param reminder
     */
    void delete(Reminder reminder);

    /**
     * 批量删除
     * @param reminders
     */
    void delete(List<Reminder> reminders);

    /**
     * 更新一条提醒
     * @param oldOne-the old reminder that should be replaced
     * @param newOne-a new reminder
     */
    void update(Reminder oldOne,Reminder newOne);

    /**
     * 查询一个日程的所有提醒
     * @param schedule
     * @return 记得判空
     */
    List<Reminder> getRemindersBySchedule(Schedule schedule);

    /**
     * 根据name模糊查询
     * @param name 关键词
     * @return 记得做判空检查
     */
    List<Reminder> findByName(String name);

    /**
     * 列出所有reminder，按照插入顺序
     * @return 记得做判空检查
     */
    @Deprecated
    List<Reminder> findAll();

    /**
     * 根据时间搜索下一个reminder
     * @param former
     * @return 没找到会返回null
     */
    Reminder findNext(LocalDateTime former);

    /**
     * 根据id查找提醒
     * @param id
     * @return
     */
    Reminder findById(int id);
}
