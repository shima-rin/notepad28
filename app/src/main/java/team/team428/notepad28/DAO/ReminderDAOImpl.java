package team.team428.notepad28.DAO;

import org.litepal.LitePal;
import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;

import java.time.LocalDateTime;
import java.util.List;

public class ReminderDAOImpl implements ReminderDAO {
    // TODO: 2021/12/16 check 
    @Override
    public void insert(Reminder reminder, Schedule schedule) {
        if (schedule.isSaved()) {
            reminder.setScheduleId(schedule.getId());
            reminder.save();
        } else {
            int scheduleId = LitePal.where("name =? and begintime=? and endtime=? and content=?")
                    .find(Schedule.class).get(0).getId();
            reminder.setId(scheduleId);
            reminder.save();
        }
    }

    // TODO: 2021/12/16 check 
    @Override
    public void insert(List<Reminder> newList, Schedule schedule) {
        LitePal.beginTransaction();
        try {
            List<Reminder> oldList = getRemindersBySchedule(schedule);
            for (Reminder reminder : newList
            ) {
                if (reminder.getId() == 0) {
                    insert(reminder, schedule);
                } else {
                    reminder.save();
                    oldList.removeIf(oldReminder -> oldReminder.getId() == reminder.getId());
                }
            }
            for (Reminder reminder : oldList
            ) {
                reminder.delete();
            }
            LitePal.setTransactionSuccessful();
        } finally {
            LitePal.endTransaction();
        }
    }

    //    checked
    @Override
    public void delete(Reminder reminder) {
        reminder.delete();
    }

    // TODO: 2021/12/7 check
    @Override
    public void delete(List<Reminder> reminders) {
        try {
            LitePal.beginTransaction();
            for (Reminder reminder : reminders
            ) {
                reminder.delete();
            }
            LitePal.setTransactionSuccessful();
        } finally {
            LitePal.endTransaction();
        }
    }

    //    checked
    @Override
    public void update(Reminder oldOne, Reminder newOne) {
        oldOne.setId(newOne.getId());
        oldOne.setName(newOne.getName());
        oldOne.setScheduleId(newOne.getScheduleId());
        oldOne.setTime(newOne.getTime());
        oldOne.setContent(newOne.getContent());
        oldOne.save();
    }

    //    checked
    @Override
    public List<Reminder> findByName(String name) {
        return LitePal.where("name like ?", "%" + name + "%").find(Reminder.class);
    }

    // TODO: 2021/12/7 check
    @Override
    public List<Reminder> getRemindersBySchedule(Schedule schedule) {
        return LitePal.where("scheduleid = ?", Integer.toString(schedule.getId())).find(Reminder.class);
    }

    //    checked
    @Override
    public List<Reminder> findAll() {
        return LitePal.findAll(Reminder.class);
    }

    //    checked
    @Override
    public Reminder findNext(LocalDateTime former) {
        List<Reminder> reminders =
                LitePal.where("time > ?", Utils.LocalDateTimeToString(former))
                        .order("time")
                        .limit(1)
                        .find(Reminder.class);

        if (reminders.isEmpty()) {
            return null;
        } else {
            return reminders.get(0);
        }
    }

    // TODO: 2021/12/15 check 
    @Override
    public Reminder findById(int id) {
        return LitePal.where("id=?", String.valueOf(id)).find(Reminder.class).get(0);
    }
}
