package team.team428.notepad28.DAO;

public class DAOFactory {
    public static ScheduleDAO getScheduleDaoInstance(){
        return new ScheduleDAOImpl();
    }

    public static ReminderDAO getReminderDaoInstance(){
        return new ReminderDAOImpl();
    }
}
