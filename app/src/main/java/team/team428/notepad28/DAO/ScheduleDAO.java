package team.team428.notepad28.DAO;

import java.util.List;

import team.team428.notepad28.Bean.Reminder;
import team.team428.notepad28.Bean.Schedule;

public interface ScheduleDAO {
    /**
     * 插入一条日程
     * @param schedule
     */
    void insert(Schedule schedule);

    /**
     * 删除一条日程
     * @param schedule
     */
    void delete(Schedule schedule);

    /**
     * 批量删除
     * @param schedules
     */
    void delete(List<Schedule> schedules);

    /**
     * 更新一条日程
     * @param oldOne
     * @param newOne
     */
    void update(Schedule oldOne,Schedule newOne);

    /**
     * 查找提醒的日程
     * @param reminder
     * @return
     */
    Schedule findByReminder(Reminder reminder);

    /**
     * 根据关键词模糊查询
     * @param name
     * @return list记得判空
     */
    List<Schedule> findByName(String name);

    /**
     * 列出所有日程，按照插入顺序
     * @return list记得判空
     */
    List<Schedule> findAll();

    /**
     * 列出所有日程，按照起始时间排序
     * @return list记得判空
     */
    List<Schedule> findAllOrderByStartTime();

    /**
     * 根据id查找日程
     * @param id
     * @return
     */
    Schedule findById(int id);
}
