package team.team428.notepad28;

import org.junit.Test;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import team.team428.notepad28.DAO.Utils;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(Utils.LocalDateTimeToString(localDateTime));
        System.out.println(Utils.StringToLocalDateTime("2012-11-12 12:22").toString());
    }
}